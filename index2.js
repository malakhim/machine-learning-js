// Machine Learning tutorial, from https://hackernoon.com/machine-learning-with-javascript-part-1-9b97f3ed4fe5

const ml = require('ml-regression');
const csv = require('csvtojson');
const SLR = ml.SLR; // Simple Linear Regression

const csvFilePath = 'advertising.csv';
let csvData = [],
    X = [],
    y = [];

let regressionModel;

/**
 * @description Parses CSV into JSON and gets the data point from the JSON objects
 */
csv()
    .fromFile(csvFilePath)
    .on('json', (jsonObj) => {
        csvData.push(jsonObj);
    })
    .on('done', () => {
        dressData(); // Gets data points from json objects
        performRegression();
    });

/**
 * @description Extract the relevant data from the json objects
 */
dressData = () => {
   /**
     * One row of the data object looks like:
     * {
     *   TV: "10",
     *   Radio: "100",
     *   Newspaper: "20",
     *   "Sales": "1000"
     * }
     *
     * Hence, while adding the data points,
     * we need to parse the String value as a Float.
     */
    csvData.forEach((row) => {
        X.push(f(row.Radio));
        y.push(f(row.Sales));
    });    
}

/**
 * @description Our model requires floating point precision, so this utility function does just that
 */
f = (s) => {
    return parseFloat(s);
}

/**
 * @description Calculate simple linear regression on the data points (Arrays of X and y)
 */
performRegression = () => {
    regressionModel = new SLR(X, y);
    console.log(regressionModel.toString(3));
    predictOutput();
}

/**
 * @description Extract out the predicted value
 */
predictOutput = () => {
    rl.question('Enter input X for prediction (Press Ctrl+C to exit) : ', (answer) => {
        console.log(`At X = ${answer}, y = ${regressionModel.predict(parseFloat(answer))}`);
        predictOutput();
    });
}

const readLine = require('readline');
const rl = readLine.createInterface({
    input: process.stdin,
    output: process.stdout
});